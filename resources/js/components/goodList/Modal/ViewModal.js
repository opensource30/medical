import React, { Component } from 'react';



class ViewModal extends Component{
  
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            
            <div className="modal fade" id={this.props.modal_id} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel"> {this.props.goodData.selected_good.generic_name} - {this.props.goodData.selected_good.name}</h5>
                    <button typeName="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    
                   <table className="center-table-show">
                        <tr><td>ID</td><td>{this.props.goodData.selected_good.id} </td></tr>
                            <tr><td>Name</td><td><strong> {this.props.goodData.selected_good.name}</strong></td></tr>
                            <tr><td>Generic Name</td><td><strong> {this.props.goodData.selected_good.generic_name}</strong></td></tr>

                            <tr><td>Code</td><td> {this.props.goodData.selected_good.product_code}</td></tr>
                            <tr><td>Category</td><td> {this.props.goodData.selected_good.product_category}</td></tr>
                            <tr><td>MRP</td><td> {this.props.goodData.selected_good.mrp_price}</td></tr>
                            <tr><td>Vendor Price</td><td> {this.props.goodData.selected_good.vendor_price}</td></tr>
                            <tr><td>Packet to Stripe</td><td> {this.props.goodData.selected_good.packet_total_stripe}</td></tr>
                            <tr><td>Stripe to Unit</td><td> {this.props.goodData.selected_good.stripe_total_unit}</td></tr>
                            
                            <tr><td>Packet to Unit</td><td> {this.props.goodData.selected_good.packet_total_unit}</td></tr>
                            
                            <tr><td>Created</td><td>{this.props.goodData.selected_good.created_at}</td></tr>
                    </table>


                
                
                
                
                </div>

                 <select name="unittype" id="unittype">
                        <option value="pcs" selected>pcs</option>
                        <option value="strips">strips</option>
                        <option value="boxes">boxes</option>
                 </select>
                <div className="modal-footer">
                    <button type="button" className="btn btn-primary">Add To Cart</button>
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>

                </div>
                </div>
            </div>
            </div>
            
            
            
            
    
            
            
        );

    }

}


export default ViewModal;





