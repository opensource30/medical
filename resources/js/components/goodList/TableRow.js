import React, { Component } from 'react';
import TableButtonActions from './TableButtonActions';



class TableRow extends Component{
  
    constructor(props){
        super(props);
        this.state={

            goods: [],
        }
    }
    
    render(){
        return (
            <tr>
            <td>{this.props.data.id}</td>
            <td>{this.props.data.product_code} </td>
            <td>{this.props.data.name}</td>
            <td>{this.props.data.generic_name}</td>
           
            <td>{this.props.data.product_category}</td>

            <td>{this.props.data.mrp_price}</td>
            <td>{this.props.data.packet_total_stripe}</td>
            <td>{this.props.data.stripe_total_unit}</td>
            <td><TableButtonActions eachRow={this.props.data} /></td>
            
            </tr>
            
            
        );

    }

}


export default TableRow;





