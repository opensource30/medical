import axios from 'axios';
import React, { Component } from 'react';
import ViewModal from './Modal/ViewModal';
import EditModal from './Modal/EditModal';
import { useHistory } from "react-router-dom"; 




class TableButtonActions extends Component{
  
    constructor(props){
        super(props);
        this.state ={
            selected_good: {},


        }
    }
    
    
    getGoodDetails=(medicine) => {
       
        this.setState({
            selected_good: medicine,
        
      });
       
        /*
       console.log(id);
     
        axios.get('api/goods/'+id)
        .then((response) => {
            console.log(response.data);
            this.setState({
                selected_name: response.data.good.name,
            
          });
          
        });*/
        
    } 
   
    editGoodDetails=(medicine) => {
       console.log("redirect should work");
        this.setState({
            selected_good: medicine,

      });
 
    //window.location.replace('/cart')
   
    } 






    render(){
        return (
           
            <div className="btn-group" role="group">
                <button type="button" 
                className="btn btn-primary" 
                data-bs-toggle="modal" 
                data-bs-target={'#viewModal'+this.props.eachRow.id} 
                onClick={()=>{this.getGoodDetails(this.props.eachRow)}}
                
                >View</button>
                <ViewModal modal_id={'viewModal'+this.props.eachRow.id} goodData={this.state} />
                
                
                
             <button type="button" 
                className="btn btn-info" 
                data-bs-toggle="modal" 
                data-bs-target={'#editModal'+this.props.eachRow.id}  
                 onClick={()=>{this.editGoodDetails(this.props.eachRow)}}>Edit</button>


                <EditModal modal_id={'editModal'+this.props.eachRow.id} goodData={this.state} />

                <button type="button" className="btn btn-danger">Delete</button>
            </div>
            
           
            
            
        );

    }

}


export default TableButtonActions;





