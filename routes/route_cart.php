<?php //breeze 
use App\Http\Controllers\CartController;

Route::middleware([auth::class])->group(function () {
Route::get('/cart',[CartController::class,'index'])->name('cart.index');
Route::post('/cart',[CartController::class,'add'])->name('cart.add');
Route::post('/cart/conditions',[CartController::class,'addCondition'])->name('cart.addCondition');
Route::delete('/cart/conditions',[CartController::class,'clearCartConditions'])->name('cart.clearCartConditions');
Route::get('/cart/details',[CartController::class,'details'])->name('cart.details');
Route::delete('/cart/{id}',[CartController::class,'delete'])->name('cart.delete');
Route::post('/cart/clear',[CartController::class,'clear'])->name('cart.clear');

})

?>