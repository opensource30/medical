<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API_GoodController;
use App\Http\Controllers\API_CartController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('goods',[API_GoodController::class,'index'])->name('goods.index');
Route::get('goods/{good}',[API_GoodController::class,'show'])->name('goods.show');
//Route::get('goods/{id}',[API_GoodController::class,'showUsingID'])->name('goods.showUsingID');

Route::post('goods',[API_GoodController::class,'store'])->name('goods.store');
Route::delete('goods/{id}',[API_GoodController::class,'destroy'])->name('goods.destroy');
Route::get('/cart',[API_CartController::class,'index'])->name('cart_user.index');
