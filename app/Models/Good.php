<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{

    use HasFactory;

    protected $fillable = [
        'name','product_code','product_category','mrp_price','vendor_price','packet_total_unit','stripe_total_unit','packet_total_stripe','generic_name'
    ];
}
