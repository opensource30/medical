<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 4/30/2017
 * Time: 10:58 AM
 */

namespace App\Http\Controllers;
use App\Models\DatabaseStorageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Darryldecode\Cart\CartCondition;

class API_CartController extends Controller
{
    public function index($userid)
    {
        $userId = $userid; // get this from session or wherever it came from
        
        $items = [];

            \Cart::session($userId)->getContent()->each(function($item) use (&$items)
            {
                $items[] = $item;
            });
        
        return response()->json(['success' => true,'data' => $items,
        'total_quantity' => \Cart::session($userId)->getTotalQuantity(),
        'sub_total' => \Cart::session($userId)->getSubTotal(),
        'total' => \Cart::session($userId)->getTotal(),
        'message' => 'cart get item  successfully'
        ]) ;
   
    }

    public function add()
    {
        $userId = Auth::id(); // get this from session or wherever it came from

        $id = request('id');
        $name = request('name');
        $price = request('price');
        
        $stripe_total_unit = request('stripe_total_unit');
        $packet_total_stripe = request('packet_total_stripe');
        $unittype =request('unittype');
        $qty = request('qty');
        
        //$items[]= \Cart::session($userId)->get($id);
        $itemSelected =\Cart::session($userId)->get($id); // added item returned 
        
        $box_qty =0;
        $strip_qty=0;
        $pcs_qty=0;
        $json_qty=0;
        $msg =""; 
        $valid=true;
        $total_pc_count=0;
        //dd($itemSelected);

        if(!empty($itemSelected)){
        $assoc = $itemSelected["attributes"];

            foreach ($assoc as $key => $value) {
                
                if($key=='box_attr'){
                    $box_qty = $value["qty"];
                }    
                elseif($key=='strips_attr'){
                    $strip_qty = $value["qty"];
                }
                elseif($key=='pcs_attr'){
                    $pcs_qty = $value["qty"];

                } 
            }

        }

        if($unittype=='boxes') {
                    
            $customAttributes = [
                'box_attr' => [
                    'label' => 'boxes',
                    'qty' => $qty+$box_qty,
                ],
                'strips_attr' => [
                    'label' => 'strips',
                    'qty' => $strip_qty,
                ],
                'pcs_attr' => [
                    'label' => 'pcs',
                    'qty' => $pcs_qty,
                ],
            ];
            
            $json_qty = $qty+$box_qty;
                if($json_qty>5){
                    $valid =false;
                    $msg = "Number of Packet should be 1-5.";
                }
                $qty = request('qty')* $stripe_total_unit * $packet_total_stripe; // converted to pcs

        } else if($unittype=='strips'){
            
            $customAttributes = [
                'box_attr' => [
                    'label' => 'boxes',
                    'qty' => $box_qty,
                ],
                'strips_attr' => [
                    'label' => 'strips',
                    'qty' => $qty+$strip_qty,
                ],
                'pcs_attr' => [
                    'label' => 'pcs',
                    'qty' => $pcs_qty,
                ],
            ];

            $json_qty = $qty+$strip_qty;

            if($json_qty>20){
                $valid =false;
                $msg = "Number of Strips should be 1-20";
             }
             $qty = request('qty')* $stripe_total_unit; // converted to pcs

        } else if($unittype=='pcs'){
            $customAttributes = [
                'box_attr' => [
                    'label' => 'boxes',
                    'qty' => $box_qty,
                ],
                'strips_attr' => [
                    'label' => 'strips',
                    'qty' => $strip_qty,
                ],
                'pcs_attr' => [
                    'label' => 'pcs',
                    'qty' => $qty+$pcs_qty,
                ],
            ];

            $json_qty = $qty+$pcs_qty;
            if($json_qty>200){
                $valid =false;
                $msg = "Number of PCs should be 1-200";
  
              }
//            $qty = request('qty'); // converted to pcs
        }

        

      $current_total_qty = $qty + ($box_qty* $stripe_total_unit * $packet_total_stripe)+
      ($strip_qty * $stripe_total_unit)+$pcs_qty; // after adding what will be total quantity

       if($current_total_qty>200){
        $valid =false;
        $msg = "You are exceeding total ".$current_total_qty ."of medicine";
       } 

      if(!$valid)
       return redirect()->back()->with('message', $msg);            
   
        $item = \Cart::session($userId)->add($id, $name, $price, $qty, $customAttributes);
        
        $items = [];

            \Cart::session($userId)->getContent()->each(function($item) use (&$items)
            {
                $items[] = $item;
            });

            
      
    	    return view('cart',['success' => true,
            'data' => $items,
            'total_quantity' => \Cart::session($userId)->getTotalQuantity(),
            'sub_total' => \Cart::session($userId)->getSubTotal(),
            'total' => \Cart::session($userId)->getTotal(),
            'message' => 'cart item added successfully'
        ]);

    }

    /*reset cart and view all cart items*/
    public function clear()
    {
        $userId = Auth::id();
        \Cart::session($userId)->clear();
      //  return redirect()->route('cart.index', ['message' => 'cart reset to empty']);
       return redirect()->back()->with('message', "cart reset to empty");            

    
    }
    
    
    public function addCondition()
    {
        $userId = Auth::id(); // get this from session or wherever it came from

        /** @var \Illuminate\Validation\Validator $v */
        $v = validator(request()->all(),[
            'name' => 'required|string',
            'type' => 'required|string',
            'target' => 'required|string',
            'value' => 'required|string',
        ]);

        if($v->fails())
        {
            return response(array(
                'success' => false,
                'data' => [],
                'message' => $v->errors()->first()
            ),400,[]);
        }

        $name = request('name');
        $type = request('type');
        $target = request('target');
        $value = request('value');

        $cartCondition = new CartCondition([
            'name' => $name,
            'type' => $type,
            'target' => $target, // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => $value,
            'attributes' => array()
        ]);

        \Cart::session($userId)->condition($cartCondition);

        return response(array(
            'success' => true,
            'data' => $cartCondition,
            'message' => "condition added."
        ),201,[]);
    }

    public function clearCartConditions()
    {
        $userId = Auth::id(); // get this from session or wherever it came from

        \Cart::session($userId)->clearCartConditions();

        return response(array(
            'success' => true,
            'data' => [],
            'message' => "cart conditions cleared."
        ),200,[]);
    }

    public function delete($id)
    {
        $userId = Auth::id(); // get this from session or wherever it came from


        \Cart::session($userId)->remove($id);
        return redirect()->back()->with('message', "cart item {$id} removed");            

    
    
    }

    public function details()
    {
        $userId = Auth::id(); // get this from session or wherever it came from

        // get subtotal applied condition amount
        $conditions = \Cart::session($userId)->getConditions();


        // get conditions that are applied to cart sub totals
        $subTotalConditions = $conditions->filter(function (CartCondition $condition) {
            return $condition->getTarget() == 'subtotal';
        })->map(function(CartCondition $c) use ($userId) {
            return [
                'name' => $c->getName(),
                'type' => $c->getType(),
                'target' => $c->getTarget(),
                'value' => $c->getValue(),
            ];
        });

        // get conditions that are applied to cart totals
        $totalConditions = $conditions->filter(function (CartCondition $condition) {
            return $condition->getTarget() == 'total';
        })->map(function(CartCondition $c) {
            return [
                'name' => $c->getName(),
                'type' => $c->getType(),
                'target' => $c->getTarget(),
                'value' => $c->getValue(),
            ];
        });

        return response(array(
            'success' => true,
            'data' => array(
                'total_quantity' => \Cart::session($userId)->getTotalQuantity(),
                'sub_total' => \Cart::session($userId)->getSubTotal(),
                'total' => \Cart::session($userId)->getTotal(),
                'cart_sub_total_conditions_count' => $subTotalConditions->count(),
                'cart_total_conditions_count' => $totalConditions->count(),
            ),
            'message' => "Get cart details success."
        ),200,[]);
    }
}