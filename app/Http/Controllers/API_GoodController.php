<?php

namespace App\Http\Controllers;

use App\Models\Good;
use Illuminate\Http\Request;
use Log;
use Exception;

class API_GoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		$goodlist=Good::orderBy('created_at','desc')->get(); 
        
		
    	return response()->json(["success"=>true,"message"=>"successfuly data retrived",'goodlist'=>$goodlist]) ;
	
	
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
		$good = new Good;
		// 
		$good->name = $request->name;
        $good->generic_name = $request->generic_name;
        $good->product_code = $request->product_code;
		$good->product_category = $request->product_category;
        $good->mrp_price = $request->mrp_price;
        $good->vendor_price = $request->vendor_price;
        $good->packet_total_unit = $request->packet_total_unit;
        $good->stripe_total_unit = $request->stripe_total_unit;
        $good->packet_total_stripe = $request->packet_total_stripe;


        $good->save(); 
		
	    //return view('good.store',['good'=>$good]);
        return response()->json(["success"=>true,"message"=>"successfuly data save",'good'=>$good]) ;

	}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
  public function show(Good $good)
    {
        $good= Good::find($good->id);
        return response()->json(["success"=>true,"message"=>"successfuly data retrived object passed",'good'=>$good]) ;
    }

   public function showUsingID($id)
    {
        
		$good = Good::find($id);
    	return response()->json(["success"=>true,"message"=>"successfuly data retrived id passed",'good'=>$good]) ;
	
	
       // return response()->json(["success"=>true,"message"=>"successfuly data retrived",'good'=>Good::findOrFail($id)]) ;
    }



    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function edit(Good $good)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Good $good)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Good::destroy($id);
        if ($res) {
            return response()->json([
                "success" => true,
                "message" => "successfully deleted the record"
            ]);
        } else {
            return response()->json([
                "success" => false,
                "message" => "Failed to delete the record"
           
            ]);
        }
    }
}
