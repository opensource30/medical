<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     

        Schema::create('goods', function (Blueprint $table) {
            $table->id();
			$table->string('name',200)->unique();
			$table->string('product_code',200)->unique();
			$table->string('product_category',200);
            $table->string('generic_name',200);
			$table->decimal('mrp_price',10,2);
			$table->decimal('vendor_price',10,2);
            $table->integer('packet_total_unit');
            $table->integer('stripe_total_unit');
            $table->integer('packet_total_stripe');
            $table->timestamp('created_at')->useCurrent();
			$table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
};
